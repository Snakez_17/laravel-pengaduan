@extends('app')

@section('content')
    <div class="container">
        <h1>Create complaint</h1>
        <form action="/student/complaints" method="POST">
            @csrf
            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="complaint_date" class="form-label">Complaint_date</label>
                    <input type="date" class="form-control" id="complaint_date" name="complaint_date">
                </div>
            </div>
            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="content_report" class="form-label">Content_report</label>
                    <input type="text" class="form-control" id="content_report" name="content_report">
                </div>
            </div>
            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="photo" class="form-label">Photo</label>
                    <input type="file" class="form-control" id="photo" name="photo" accept="image/png,image/jpeg">
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Simpan</button>
            <button type="reset" class="btn btn-secondary">Reset</button>
        </form>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error }}</p>
            @endforeach
        @endif
    </div>
@endsection
