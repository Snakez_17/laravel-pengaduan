@extends('app')

@section('content')
    <div class="container">
        <h1>Create user</h1>
        <form action="/admin/users" method="POST">
            @csrf
            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="name" class="form-label">Name</label>
                    <input type="text" class="form-control" id="name" name="name">
                </div>
            </div>
            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="gender" class="form-label">Gender</label>
                    <input type="text" class="form-control" id="gender" name="gender">
                </div>
            </div>
            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="date_of_birth" class="form-label">Date_of_birth</label>
                    <input type="date" class="form-control" id="date_of_birth" name="date_of_birth">
                </div>
            </div>
            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="username" class="form-label">Username</label>
                    <input type="text" class="form-control" id="username" name="username">
                </div>
            </div>

            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="password" class="form-label">Password</label>
                    <input type="password" class="form-control" id="password" name="password">
                </div>
            </div>
            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="email" class="form-label">Email</label>
                    <input type="text" class="form-control" id="email" name="email">
                </div>
            </div>

            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="phone" class="form-label">Phone</label>
                    <input type="text" class="form-control" id="phone" name="phone">
                </div>
            </div>

            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="address" class="form-label">Address</label>
                    <input type="text" class="form-control" id="address" name="address">
                </div>
            </div>

            <div class="col-3 mb-3">
                <label class="form-label">Level</label>
                <select name="level" class="form-select">
                    @foreach (['admin', 'officer', 'student'] as $item)
                        <option value="{{ $item }}">
                            {{ $item }}</option>
                    @endforeach
                </select>
            </div>

            <button type="submit" class="btn btn-primary">Simpan</button>
            <button type="reset" class="btn btn-secondary">Reset</button>
        </form>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error }}</p>
            @endforeach
        @endif
    </div>
@endsection
