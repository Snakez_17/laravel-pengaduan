@extends('app')

@section('content')
    <div class="container">
        <h1>ADMIN USER</h1>
        <p>{{ $user_list->links() }}</p>
        <table class="table">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Name</th>
                    <th>Gender</th>
                    <th>Date_of_birth</th>
                    <th>Username</th>
                    <th>Phone</th>
                    <th>Address</th>
                    <th>Level</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($user_list as $user)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->gender }}</td>
                        <td>{{ $user->date_of_birth }}</td>
                        <td>{{ $user->username }}</td>
                        <td>{{ $user->phone }}</td>
                        <td>{{ $user->address }}</td>
                        <td>{{ $user->level }}</td>
                        <td>
                            <a href="/admin/users/{{ $user->id }}" class="btn btn-primary">Detail</a>
                            <form action="/admin/users/{{ $user->id }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger">Hapus</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <a href="/admin/users/create" class="btn btn-success">Tambah</a>
    </div>
@endsection
