@extends('app')

@section('content')
    <div class="container">
        <h1>Detail student</h1>
        <form action="/admin/students/{{ $student->id }}" method="POST">
            @csrf
            @method('PATCH')
            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="nisn" class="form-label">Nisn</label>
                    <input type="text" class="form-control" id="nisn" name="nisn" value="{{ $student->nisn }}"
                        disabled>
                </div>
            </div>

            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="class" class="form-label">Class</label>
                    <input type="text" class="form-control" id="class" name="class" value="{{ $student->class }}">
                </div>
            </div>

            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="user_id" class="form-label">User_id</label>
                    <input type="text" class="form-control" id="user_id" name="user_id"
                        value="{{ $student->user_id }}">
                </div>
            </div>

            <button type="submit" class="btn btn-primary">Simpan</button>
            <button type="reset" class="btn btn-secondary">Reset</button>
        </form>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <p class="text-danger">{{ $error }}</p>
            @endforeach
        @endif
    </div>
@endsection
