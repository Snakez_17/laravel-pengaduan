@extends('app')

@section('content')
    <div class="container">
        <h1>ADMIN COMPLAINT</h1>
        <p>{{ $complaint_list->links() }}</p>
        <table class="table">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Complaint_date</th>
                    <th>User_id</th>
                    <th>Content_report</th>
                    <th>Photo</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($complaint_list as $complaint)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $complaint->complaint_date }}</td>
                        <td>{{ $complaint->user_id }}</td>
                        <td>{{ $complaint->content_report }}</td>
                        <td>{{ $complaint->photo }}</td>
                        <td>{{ $complaint->status }}</td>
                        <td>
                            <a href="/admin/complaints/{{ $complaint->id }}" class="btn btn-primary">Detail</a>
                            <form action="/admin/complaints/{{ $complaint->id }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger">Hapus</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <a href="/admin/complaints/create" class="btn btn-success">Tambah</a>
    </div>
@endsection
