@extends('app')

@section('content')
    <div class="container">
        <h1>Laporan Complaint</h1>
        <form action="/admin/complaint-report" method="GET">
            <div class="row">
                <label for="start" class="col-1 col-form-label">Dari</label>
                <div class="col-2">
                    <input type="date" class="form-control" id="start" name="start" value="{{ $start }}">
                </div>
                <label for="end" class="col-1 col-form-label">Sampai</label>
                <div class="col-2">
                    <input type="date" class="form-control" id="end" name="end" value="{{ $end }}">
                </div>
                <div class="col-1">
                    <button type="submit" class="btn btn-success">Cari</button>
                </div>
            </div>
        </form>
        <table class="table">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Complaint_date</th>
                    <th>User_id</th>
                    <th>Content_report</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($complaint_list as $complaint)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $complaint->complaint_date }}</td>
                        <td>{{ $complaint->user_id }}</td>
                        <td>{{ $complaint->content_report }}</td>
                        <td>{{ $complaint->status }}</td>
                        <td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <button type="submit" class="btn btn-secondary d-print-none" onclick="window.print()">
            Print
        </button>
    </div>
@endsection
