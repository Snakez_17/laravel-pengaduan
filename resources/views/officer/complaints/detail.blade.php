@extends('app')

@section('content')
    <div class="container">
        <h1>Detail complaint</h1>
        <form action="/officer/complaints/{{ $complaint->id }}" method="POST">
            @csrf
            @method('PATCH')
            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="complaint_date" class="form-label">Complaint_date</label>
                    <input type="text" class="form-control" id="complaint_date" name="complaint_date"
                        value="{{ $complaint->complaint_date }}" disabled>
                </div>
            </div>

            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="user_id" class="form-label">User_id</label>
                    <input type="text" class="form-control" id="user_id" name="user_id"
                        value="{{ $complaint->user_id }}" disabled>
                </div>
            </div>

            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="content_report" class="form-label">Content_report</label>
                    <input type="text" class="form-control" id="content_report" name="content_report"
                        value="{{ $complaint->content_report }}" disabled>
                </div>
            </div>

            <div class="row flex-column">
                <div class="col-3 mb-3">
                    <label for="photo" class="form-label">Photo</label>
                    <input type="text" class="form-control" id="photo" name="photo" value="{{ $complaint->photo }}"
                        disabled>
                </div>
            </div>

            <div class="col-3 mb-3">
                <label class="form-label">Status</label>
                <select name="status" class="form-select">
                    @foreach (['new', 'verifed', 'reject', 'done'] as $item)
                        <option value="{{ $item }}" {{ $complaint->status == $item ? 'selected' : '' }}>
                            {{ $item }}</option>
                    @endforeach
                </select>
            </div>

            <button type="submit" class="btn btn-primary">Simpan</button>
            <button type="reset" class="btn btn-secondary">Reset</button>
        </form>
    </div>
@endsection
