@extends('app')

@section('content')
    <div class="container">
        <h1>OFFICER RESPONSE</h1>
        <p>{{ $response_list->links() }}</p>
        <table class="table">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Complaint_id</th>
                    <th>Response_date</th>
                    <th>Response</th>
                    <th>Officer_id</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($response_list as $response)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $response->complaint_id }}</td>
                        <td>{{ $response->response_date }}</td>
                        <td>{{ $response->response }}</td>
                        <td>{{ $response->officer_id }}</td>
                        <td>
                            <a href="/officer/responses/{{ $response->id }}" class="btn btn-primary">Detail</a>
                            <a href="a" class="btn btn-danger">Delete</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <a href="/officer/responses/create" class="btn btn-success">Tambah</a>
    </div>
@endsection
