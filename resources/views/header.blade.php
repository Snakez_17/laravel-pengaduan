<header class="border-bottom" id="navigation-bar">
    <div class="container d-flex align-items-center d-print-none">
        <a href="/#" class="d-flex align-items-center me-3 text-decoration-none">
            <i class="bi bi-browser-chrome d-print-none" style="font-size: 20px;"></i>
            <span class="fs-3 ms-2 d-print-none" id="aplikasi">Mebull</span>
        </a>
        <ul class="nav me-auto d-print-none">
            @if (auth()->user() && auth()->user()->level == 'admin')
                <li><a href="/admin/users" class="nav-link active" style="font-size: 20px" id="user">User</a>
                <li><a href="/admin/students" class="nav-link active" style="font-size: 20px";
                        id="student">Student</a>
                <li><a href="/admin/complaints" class="nav-link active"style="font-size: 20px";
                        id="complaint">Complaint</a>
                <li><a href="/admin/responses" class="nav-link active"
                        style="font-size: 20px;  id="response">Response</a>
                <li><a href="/admin/complaint-report" class="nav-link active"style="font-size: 20px";
                        id="generate">Generate Complaint</a>
        </ul>
        @endif
        @if (auth()->user() == null)
            <ul class="nav ms-auto d-print-none">
                <li><a href="/login" class="nav-link active" id="login"><i class="bi bi-box-arrow-in-right"
                            style="font-size: 20px"></i>
                        Login</a></li>
            </ul>
        @endif
        @if (auth()->user() != null)
            <div class="dropdown d-print-none" id="user">
                <a href="#" class="dropdown-toggle text-decoration-none" data-bs-toggle="dropdown">
                    <i class="bi bi-person-circle" style="font-size: 30px"></i>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="logout" class="dropdown-item">Log Out</a></li>
                </ul>
            </div>
        @endif
    </div>
    <div class="d-print-none" id="laporan">
        <h2 href="/form" class="text-center" style="color: white">LAPORAN PENGADUAN SEKOLAH GRACIA</h2>
    </div>
</header>
