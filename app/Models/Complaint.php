<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Complaint extends Model
{
    protected $fillable = [
        'complaint_date',
        'user_id',
        'content_report',
        'photo',
        'status',
        'created_at',
        'updated_at',
    ];
}
