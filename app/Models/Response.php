<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Response extends Model
{
    protected $fillable = [
        'complaint_id',
        'response_date',
        'response',
        'officer_id',
        'created_at',
        'updated_at',
    ];
}
