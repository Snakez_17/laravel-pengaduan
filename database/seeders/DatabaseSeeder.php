<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Student;
use App\Models\Complaint;
use App\Models\Response;

class DatabaseSeeder extends Seeder
{
    
    public function run()
    {
        {
            $user_1 = User::create([
            'name' => 'Jimmy Neutral',
            'gender' => 'male',
            'date_of_birth' => '2000-12-10',
            'username' => 'Netral_10',
            'password' => Hash::make('Coba_123'),
            'email' => 'jimmyneutral@gmail.com',
            'phone' => '085688759089',
            'address' => 'Jalan Sudirman no.10',
            'level' => 'admin',
          ]);

          $user_2 = User::create([
            'name' => 'Asep Budi',
            'gender' => 'male',
            'date_of_birth' => '2000-12-10',
            'username' => 'Budi_10',
            'password' => Hash::make('Coba_123'),
            'email' => 'asepbudi@gmail.com',
            'phone' => '085688759010',
            'address' => 'Jalan Sudirman no.11',
            'level' => 'student',
          ]);
      
           $student_1 = Student::create([
              'nisn' => '0059431015',
              'class' => '10',
              'user_id' => $user_2->id,
            ]);

            $complaint_1 = Complaint::create([
                'complaint_date' => '2023-01-10',
                'content_report' => 'pembullyan',
                'photo' => 'hhh',
                'status' => 'new',
                'user_id' => $user_1->id,
              ]);

              $response_1 = Response::create([
                'complaint_id' => $complaint_1->id,
                'response_date' => '2023-01-15',
                'response' => 'Akan ditindak lanjuti',
                'officer_id' => $user_1->id,
              ]);    

              $user_3 = User::create([
                'name' => 'Alexander Mamad',
                'gender' => 'male',
                'date_of_birth' => '2000-09-10',
                'username' => 'Alexander_10',
                'password' => Hash::make('Coba_123'),
                'email' => 'alexandermamad@gmail.com',
                'phone' => '085688759014',
                'address' => 'Jalan Sudirman no.100',
                'level' => 'admin',
              ]);
    
              $user_4 = User::create([
                'name' => 'Putra Nugroho',
                'gender' => 'male',
                'date_of_birth' => '2000-01-10',
                'username' => 'Putra_10',
                'password' => Hash::make('Coba_123'),
                'email' => 'putranugroho@gmail.com',
                'phone' => '085688759170',
                'address' => 'Jalan Sudirman no.210',
                'level' => 'student',
              ]);
          
               $student_3 = Student::create([
                  'nisn' => '0059431189',
                  'class' => '08',
                  'user_id' => $user_4->id,
                ]);
    
                $complaint_3 = Complaint::create([
                    'complaint_date' => '2022-02-10',
                    'content_report' => 'pembullyan fisik',
                    'photo' => 'ggg',
                    'status' => 'new',
                    'user_id' => $user_3->id,
                  ]);
    
                  $response_3 = Response::create([
                    'complaint_id' => $complaint_3->id,
                    'response_date' => '2022-02-15',
                    'response' => 'Akan ditindak lanjuti oleh pihak berwajib',
                    'officer_id' => $user_3->id,
                  ]);  
                  
                  $user_5 = User::create([
                    'name' => 'John Vicente',
                    'gender' => 'male',
                    'date_of_birth' => '2000-07-17',
                    'username' => 'John_10',
                    'password' => Hash::make('Coba_123'),
                    'email' => 'johnvicente@gmail.com',
                    'phone' => '085688751089',
                    'address' => 'Jalan Sudirman no.90',
                    'level' => 'admin',
                  ]);
        
                  $user_6 = User::create([
                    'name' => 'Angelica Sofie',
                    'gender' => 'female',
                    'date_of_birth' => '2004-09-24',
                    'username' => 'Sofie_10',
                    'password' => Hash::make('Coba_123'),
                    'email' => 'angelicasofie@gmail.com',
                    'phone' => '085688759778',
                    'address' => 'Jalan Kopo Katapang no.17',
                    'level' => 'student',
                  ]);
              
                   $student_5 = Student::create([
                      'nisn' => '0059431908',
                      'class' => '10',
                      'user_id' => $user_6->id,
                    ]);
        
                    $complaint_5 = Complaint::create([
                        'complaint_date' => '2020-09-17',
                        'content_report' => 'Pembullyan di sosmed',
                        'photo' => 'bbb',
                        'status' => 'new',
                        'user_id' => $user_5->id,
                      ]);
        
                      $response_5 = Response::create([
                        'complaint_id' => $complaint_5->id,
                        'response_date' => '2020-09-20',
                        'response' => 'Akan ditindak lanjuti oleh kominfo',
                        'officer_id' => $user_5->id,
                      ]);  
                      
                      $user_7 = User::create([
                        'name' => 'Titus Yudas',
                        'gender' => 'male',
                        'date_of_birth' => '2000-06-02',
                        'username' => 'Titus_10',
                        'password' => Hash::make('Coba_123'),
                        'email' => 'titusyudas@gmail.com',
                        'phone' => '085688759908',
                        'address' => 'Jalan Sudirman no.50',
                        'level' => 'officer',
                      ]);
            
                      $user_8 = User::create([
                        'name' => 'Vega Endora',
                        'gender' => 'female',
                        'date_of_birth' => '2000-03-11',
                        'username' => 'Vega_11',
                        'password' => Hash::make('Coba_123'),
                        'email' => 'vegaendora@gmail.com',
                        'phone' => '08568875987',
                        'address' => 'Jalan Sudirman no.77',
                        'level' => 'student',
                      ]);
                  
                       $student_7 = Student::create([
                          'nisn' => '0059431965',
                          'class' => '07',
                          'user_id' => $user_8->id,
                        ]);
            
                        $complaint_7 = Complaint::create([
                            'complaint_date' => '2022-01-15',
                            'content_report' => 'pembullyan Mengejek orang tua',
                            'photo' => 'yyy',
                            'status' => 'new',
                            'user_id' => $user_7->id,
                          ]);
            
                          $response_7 = Response::create([
                            'complaint_id' => $complaint_7->id,
                            'response_date' => '2022-01-17',
                            'response' => 'Akan ditindak lanjuti oleh pihak sekolah',
                            'officer_id' => $user_7->id,
                          ]);  
                          
                          $user_9 = User::create([
                            'name' => 'Dante Vicente',
                            'gender' => 'male',
                            'date_of_birth' => '2000-07-07',
                            'username' => 'Dante_07',
                            'password' => Hash::make('Coba_123'),
                            'email' => 'dantevicente@gmail.com',
                            'phone' => '085688759777',
                            'address' => 'Jalan Pagarsih no.707',
                            'level' => 'officer',
                          ]);
                
                          $user_10 = User::create([
                            'name' => 'Gean Calista',
                            'gender' => 'female',
                            'date_of_birth' => '2000-08-08',
                            'username' => 'Gean_08',
                            'password' => Hash::make('Coba_123'),
                            'email' => 'geancalista@gmail.com',
                            'phone' => '085688759888',
                            'address' => 'Jalan Pagarsih no.808',
                            'level' => 'student',
                          ]);
                      
                           $student_9 = Student::create([
                              'nisn' => '0059431088',
                              'class' => '10',
                              'user_id' => $user_10->id,
                            ]);
                
                            $complaint_9 = Complaint::create([
                                'complaint_date' => '2022-08-17',
                                'content_report' => 'pembullyan Pemukulan',
                                'photo' => 'jjj',
                                'status' => 'new',
                                'user_id' => $user_9->id,
                              ]);
                
                              $response_9 = Response::create([
                                'complaint_id' => $complaint_9->id,
                                'response_date' => '2022-08-07',
                                'response' => 'Akan ditindak lanjuti',
                                'officer_id' => $user_9->id,
                              ]);    
         }
    }
}