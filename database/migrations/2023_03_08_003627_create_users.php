<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsers extends Migration
{
    
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name', 35);
            $table->enum('gender', ['male', 'female']);
            $table->date('date_of_birth');
            $table->string('username', 25)->unique();
            $table->string('password', 255);
            $table->string('email', 30)->unique();
            $table->string('phone', 32);
            $table->string('address', 100);
            $table->enum('level', ['admin', 'officer', 'student'])-> default('admin');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
